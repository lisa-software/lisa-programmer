#ifndef PARDEF_INCLUDED
#define PARDEF_INCLUDED

#include "omtcBas.h"
#include "omtcItf.h"

#ifndef PACKED
#define PACKED 
#endif

#ifndef PACKED_STRUCT
#define PACKED_STRUCT PACKED struct
#endif

#ifndef POST_PACKED
#define POST_PACKED 
#endif

extern int getRowCount(int typId);

typedef PACKED_STRUCT
{
  lsa_int Detektor_Anfo;
  lsa_float staint;
  lsa_float sluint;
  lsa_float fratid;
  lsa_float udkobl;
  lsa_float opm;
  lsa_float opmfbi;
  lsa_float maxo;
  lsa_bool gen;
} POST_PACKED _verlparParaType_;

extern _verlparParaType_ *verlpar;

void* getValueverlpar( lsa_int spalte, lsa_int zeile );
lsa_int getverlpar( lsa_int spalte, lsa_int zeile );
lsa_int verlpar_getColCount();
lsa_int verlpar_getRowCount();
Det getverlpar_Detektor_Anfo( int row );

typedef PACKED_STRUCT
{
  lsa_int Detektor_Anfo;
  lsa_float retnin;
  lsa_float udbala;
  lsa_int anmfor;
  lsa_int pastil;
  lsa_float anmses;
  lsa_int hukomm;
  lsa_float forsin;
  lsa_float udskud;
  lsa_float prrese;
} POST_PACKED _anfoparParaType_;

extern _anfoparParaType_ *anfopar;

void* getValueanfopar( lsa_int spalte, lsa_int zeile );
lsa_int getanfopar( lsa_int spalte, lsa_int zeile );
lsa_int anfopar_getColCount();
lsa_int anfopar_getRowCount();
Det getanfopar_Detektor_Anfo( int row );

typedef PACKED_STRUCT
{
  lsa_int Detektor_Funktion;
  lsa_float inteff;
  lsa_float maxfor;
  lsa_int fejl;
} POST_PACKED _funkparParaType_;

extern _funkparParaType_ *funkpar;

void* getValuefunkpar( lsa_int spalte, lsa_int zeile );
lsa_int getfunkpar( lsa_int spalte, lsa_int zeile );
lsa_int funkpar_getColCount();
lsa_int funkpar_getRowCount();
Det getfunkpar_Detektor_Funktion( int row );

typedef PACKED_STRUCT
{
  lsa_int phd;
  lsa_float min;
  lsa_float forl;
  lsa_float frat;
} POST_PACKED _pxParaType_;

extern _pxParaType_ *px;

void* getValuepx( lsa_int spalte, lsa_int zeile );
lsa_int getpx( lsa_int spalte, lsa_int zeile );
lsa_int px_getColCount();
lsa_int px_getRowCount();
Pha getpx_phd( int row );


#endif

