/**
 * OMTC Control logic.
 * test - 
 * Spjald
 * @author grote_W10
 * @version Spjald/STmDK-0009/2019.10.19
 * @lastchange grote_W10/19-10-2019  09:26:11
 * @generator LISA 7.2.0.729, SCHLOTHAUER & WAUER GmbH
 * @library $Controller: OML, $Version: 2.0, $Name: LISA OML 2.0, $File: C:/Program Files (x86)/LISA71/LISA_OMTC/StvOML200.jar
$Controller: OML, $Version: Math Library, $Name: Math Library, $File: C:/Program Files (x86)/LISA71/LISA_OMTC/MathLISA100.jar
$Controller: OML, $Version: Math Library, $Name: Math Library, $File: C:/Program Files (x86)/LISA71/LISA_OMTC/MathLISA100.jar$Controller: OML, $Version: OML Extended 1.2, $Name: OML Extended 1.2, $File: C:/Program Files (x86)/LISA71/LISA_OMTC/OMLExtended120.jar $
 */

#ifndef SPJALD_INCLUDED
#define SPJALD_INCLUDED

#include "omtcBas.h"
#include "omtcItf.h"


void Hauptlogik( void );
void UP_Anfo( void );
void Vorlogik( void );
void Umschaltung( void );
lsa_bool Logiktest( void );
lsa_int UP_Styreblokke_1( void );
void Nachlogik( void );
void Init( lsa_int initMode );
void DST_Erkennung( void );
void min_max( void );
lsa_int UP_Styreblokke_2( void );
void UP_Opma( Sgr SG, Det *Det_, lsa_float *min_Opma_ );
void akt_Dauer_phase( lsa_float *VZ_ );
lsa_bool Anf_Bel( Det *Det_, lsa_int fail_ );
lsa_bool Anf_ZL( Det Det_, lsa_int fail_, lsa_float par_ );
void DA( Det *Det_, Sgr SG, lsa_float par_Daueranfo, lsa_float par_Sperr );
lsa_bool Det_Flanke( Det Inp );
void FG_loeschen( Sgr sg, lsa_bool Anfo_FG1, lsa_bool Anfo_FG2, lsa_float para );
lsa_bool Gruen( Sgr SG );
void GZ_min_det( Sgr SG, Det Det_, lsa_float Anfangswert, lsa_float Aditionswert, lsa_float Endwert, lsa_float *Ergebniswert, lsa_int *Anz_FZ_Det );
lsa_bool NE( Pue Phasenpue );
void PhD_set( lsa_float par_ );
void SG_Wartezeit( void );
void SK( Det anf_DET1, Det anf_DET2, Sgr SG_FG, Ausg QUIT );
void Speicher_max_Wartezeit( void );
void UP_Anz_ZL( Sgr SG, Det Det_, lsa_float *ZLX );
void UP_Det_Anfo_Flanke( Det *Det_, Tim *Timer_, lsa_float par_TVZ, lsa_float fail_Det );
void UP_GZ( Sgr SG, lsa_float *GZ_SG, lsa_float min_SG, lsa_bool ASG_ );
void UP_ZZ_Zuschlag( lsa_bool *ZZH_SG, Sgr SG, lsa_float par_ZZH, Det det_, lsa_float par_zl_ );
lsa_bool VL_Det( Det Det_, lsa_float FailDet_, Sgr SG );
void Wechsel_Pue( Pue *Start_Pue, Pue *Ziel_Pue, lsa_float Putx );
lsa_bool Zwi( Sgr SGReinf, lsa_float Diff );
lsa_bool minfrei( Sgr Signal );
lsa_bool FRE( Det *Det_v, Det *Det_n, lsa_int detfail_, Sgr SG, lsa_bool *Merker_reservatiert, lsa_bool *M_Anfo );
lsa_bool Anmeldelser( Det *Det_, lsa_int detfail_, Sgr SG, lsa_bool *Merker_reservatiert );

/* Global variables */
extern lsa_bool Anfo_B1;
extern lsa_bool Anfo_B2;
extern lsa_bool Anfo_bf;
extern lsa_bool Anfo_bg;
extern lsa_bool ASG;
extern lsa_bool dummy_boo;
extern lsa_int Fail_D1;
extern lsa_int Fail_D2;
extern lsa_int Fail_D3;
extern lsa_int Fail_D4;
extern lsa_int Fail_D5;
extern lsa_int Fail_D6;
extern lsa_int Fail_D7;
extern lsa_int Fail_D8;
extern lsa_float forl;
extern lsa_float frat;
extern lsa_bool M_VL1a;
extern lsa_float MAXWZ;
extern Sgr MAXWZ_SGR;
extern lsa_float min;
extern lsa_float min_Opma_A1;
extern lsa_float min_Opma_A2;
extern lsa_int Periode;
extern lsa_int PZ;
extern lsa_float SG_WZ[20];
extern lsa_float VZ;
extern lsa_float WARTE_MAX;
extern Sgr WARTE_MAX_SG;
extern lsa_int WZ_bf;
extern lsa_int WZ_bg;
extern lsa_float Zeitluecke_D1;
extern lsa_float Zeitluecke_D2;
extern lsa_float Zeitluecke_D3;
extern lsa_float Zeitluecke_D4;
extern lsa_float Zeitluecke_D5a;
extern lsa_float Zeitluecke_D6;
extern lsa_float Zeitluecke_D7a;
extern lsa_float Zeitluecke_D8;
extern lsa_bool ZZV;
extern lsa_float Test;
extern lsa_bool ANF_D5;
extern lsa_bool ANF_D7;

/* OCIT color indications */
#undef SGR_FbRot
#define SGR_FbRot 3
#undef SGR_FbRotgelb
#define SGR_FbRotgelb 15
#undef SGR_FbGelb
#define SGR_FbGelb 12
#undef SGR_FbGruen
#define SGR_FbGruen 48
#undef SGR_FbDunkel
#define SGR_FbDunkel 0
#undef SGR_FbRotgruen
#define SGR_FbRotgruen 51
#undef SGR_FbGelbgruen
#define SGR_FbGelbgruen 60
#undef SGR_FbRotblk
#define SGR_FbRotblk 1
#undef SGR_FbGelbblk
#define SGR_FbGelbblk 4
#undef SGR_FbGruenblk
#define SGR_FbGruenblk 16
#undef SGR_FbWbl_rotgruen
#define SGR_FbWbl_rotgruen 33
#undef SGR_FbWbl_rotgelb
#define SGR_FbWbl_rotgelb 9
#undef SGR_FbWbl_gelbgruen
#define SGR_FbWbl_gelbgruen 36
#undef SGR_FbRotblk2hz
#define SGR_FbRotblk2hz 65
#undef SGR_FbGelbblk2hz
#define SGR_FbGelbblk2hz 68
#undef SGR_FbGruenblk2hz
#define SGR_FbGruenblk2hz 80
#undef SGR_FbWbl2hz_rotgruen
#define SGR_FbWbl2hz_rotgruen 97
#undef SGR_FbWbl2hz_rotgelb
#define SGR_FbWbl2hz_rotgelb 73
#undef SGR_FbWbl2hz_gelbgruen
#define SGR_FbWbl2hz_gelbgruen 100

#endif
