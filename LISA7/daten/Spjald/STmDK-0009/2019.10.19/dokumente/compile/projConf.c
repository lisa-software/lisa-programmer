#ifndef PROJCONF_INCLUDED
#define PROJCONF_INCLUDED

#include "omtcItf.h"
#include "omtcLsa.h"
#include "pardef.h"
#include "MathLISA.h"
#include "extImpl.h"
#include "extFunc.h"
#include "extPlgd.h"
#include "extPar.h"


/* Listen f�r OMTC */
TkObj tkObjs[ 1 ];
AusgObj ausgObjs[ 2 ];
SgrObj sgrObjs[ 8 ];
DetObj detObjs[ 12 ];
SplObj splObjs[ 2 ];
TimObj timObjs[ 6 ];
PhaObj phaObjs[ 2 ];
PueObj pueObjs[ 2 ];
VerfahrenObj verfahrenObjs[ 1 ];


_verlparParaType_ *verlpar;
_anfoparParaType_ *anfopar;
_funkparParaType_ *funkpar;
_pxParaType_ *px;


static TUUID uuidVerlpar = { 0x904E02B4, 0x77A2, 0x462F, { 0x8A, 0xD6, 0x34, 0xE8, 0xDB, 0x89, 0xED, 0x2D } };
static TUUID uuidAnfopar = { 0x305BA443, 0xC8BB, 0x4DD0, { 0xA3, 0xE5, 0xD8, 0xD3, 0x16, 0x06, 0x1E, 0x28 } };
static TUUID uuidFunkpar = { 0xA005243B, 0x4A26, 0x4246, { 0x8D, 0x38, 0x7C, 0xEE, 0x97, 0xBF, 0x3D, 0xFD } };
static TUUID uuidPx = { 0x7A97A93E, 0xD875, 0x4981, { 0x86, 0xD9, 0xB4, 0x00, 0x2E, 0xDD, 0xF4, 0xB2 } };


_paraElementStructureType_ _verlparParaElementStructure[9] = {
  {"Detektor_Anfo", 8, "", {0x5025DFB1, 0x71F0, 0x49C3, {0xA0, 0xC6, 0x4E, 0xDD, 0xA5, 0xC8, 0x65, 0xE5}}},
  {"staint", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"sluint", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"fratid", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"udkobl", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"opm", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"opmfbi", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"maxo", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"gen", 6, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}}
};

_paraElementStructureType_ _anfoparParaElementStructure[10] = {
  {"Detektor_Anfo", 8, "", {0x5025DFB1, 0x71F0, 0x49C3, {0xA0, 0xC6, 0x4E, 0xDD, 0xA5, 0xC8, 0x65, 0xE5}}},
  {"retnin", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"udbala", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"anmfor", 7, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"pastil", 7, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"anmses", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"hukomm", 7, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"forsin", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"udskud", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"prrese", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}}
};

_paraElementStructureType_ _funkparParaElementStructure[4] = {
  {"Detektor_Funktion", 8, "", {0x5025DFB1, 0x71F0, 0x49C3, {0xA0, 0xC6, 0x4E, 0xDD, 0xA5, 0xC8, 0x65, 0xE5}}},
  {"inteff", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"maxfor", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"fejl", 7, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}}
};

_paraElementStructureType_ _pxParaElementStructure[4] = {
  {"phd", 8, "", {0xABC971D5, 0x97D8, 0x43E6, {0xA8, 0x39, 0xA1, 0xF5, 0x5D, 0xAA, 0xC5, 0x5D}}},
  {"min", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"forl", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}},
  {"frat", 4, "", {0x00000000, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}}}
};

_parameterblockStructureType_ _parameterblockStructures[7] = {
  {{0x904E02B4, 0x77A2, 0x462F, {0x8A, 0xD6, 0x34, 0xE8, 0xDB, 0x89, 0xED, 0x2D}}, "verlpar", 3, 100, 12, 33, 9, 1, (_paraElementStructureType_**)&_verlparParaElementStructure},
  {{0x305BA443, 0xC8BB, 0x4DD0, {0xA3, 0xE5, 0xD8, 0xD3, 0x16, 0x06, 0x1E, 0x28}}, "anfopar", 3, 100, 12, 40, 10, 1, (_paraElementStructureType_**)&_anfoparParaElementStructure},
  {{0xA005243B, 0x4A26, 0x4246, {0x8D, 0x38, 0x7C, 0xEE, 0x97, 0xBF, 0x3D, 0xFD}}, "funkpar", 3, 100, 12, 16, 4, 1, (_paraElementStructureType_**)&_funkparParaElementStructure},
  {{0x42EE728E, 0xFF7E, 0x48B1, {0xB8, 0x51, 0xD5, 0xC2, 0x7F, 0xD3, 0x1D, 0x2C}}, "klass", 3, 80, 8, 6, 3, 1, (_paraElementStructureType_**)&_klassParaElementStructure},
  {{0xB6011057, 0x78F5, 0x4406, {0x89, 0x1C, 0x9D, 0x08, 0xC2, 0xDE, 0x1E, 0xE3}}, "t", 4, 80, 8, 36, 9, 1, (_paraElementStructureType_**)&_tParaElementStructure},
  {{0x5D5FEC55, 0x6EF7, 0x41D4, {0xA9, 0x75, 0x63, 0x4E, 0x1F, 0xBA, 0xED, 0xEA}}, "mst", 4, 40, 0, 23, 8, 1, (_paraElementStructureType_**)&_mstParaElementStructure},
  {{0x7A97A93E, 0xD875, 0x4981, {0x86, 0xD9, 0xB4, 0x00, 0x2E, 0xDD, 0xF4, 0xB2}}, "px", 4, 100, 2, 16, 4, 1, (_paraElementStructureType_**)&_pxParaElementStructure}
};


void* getValueverlpar( lsa_int spalte, lsa_int zeile )
{
  switch (spalte)
  {
    case 0: return detOfNr( verlpar[zeile].Detektor_Anfo );
    case 1: return &verlpar[zeile].staint;
    case 2: return &verlpar[zeile].sluint;
    case 3: return &verlpar[zeile].fratid;
    case 4: return &verlpar[zeile].udkobl;
    case 5: return &verlpar[zeile].opm;
    case 6: return &verlpar[zeile].opmfbi;
    case 7: return &verlpar[zeile].maxo;
    case 8: return &verlpar[zeile].gen;
    default: return NULL;
  }
}

lsa_int getverlpar(lsa_int spalte, lsa_int zeile) {
  void* result = getValueverlpar(spalte, zeile);
  if (result != NULL) {
    switch (spalte) {
      case 0:
        return *((lsa_int*)result);
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
        return *((lsa_float*)result);
      case 8:
        return *((lsa_bool*)result);
      default:
        return 0;
    }
  } else {
    return -1;
  }
}

lsa_int verlpar_ColCount = 9;
lsa_int verlpar_getColCount()
{
  return verlpar_ColCount;
}

lsa_int verlpar_getRowCount()
{
  return getRowCount(0);
}

Det getverlpar_Detektor_Anfo( int row )
{
  Det* result = detOfNr( verlpar[row].Detektor_Anfo );
  if (result != NULL) {
    return *result;
  } else {
    return -1;
  }
}

void* getValueanfopar( lsa_int spalte, lsa_int zeile )
{
  switch (spalte)
  {
    case 0: return detOfNr( anfopar[zeile].Detektor_Anfo );
    case 1: return &anfopar[zeile].retnin;
    case 2: return &anfopar[zeile].udbala;
    case 3: return &anfopar[zeile].anmfor;
    case 4: return &anfopar[zeile].pastil;
    case 5: return &anfopar[zeile].anmses;
    case 6: return &anfopar[zeile].hukomm;
    case 7: return &anfopar[zeile].forsin;
    case 8: return &anfopar[zeile].udskud;
    case 9: return &anfopar[zeile].prrese;
    default: return NULL;
  }
}

lsa_int getanfopar(lsa_int spalte, lsa_int zeile) {
  void* result = getValueanfopar(spalte, zeile);
  if (result != NULL) {
    switch (spalte) {
      case 0:
      case 3:
      case 4:
      case 6:
        return *((lsa_int*)result);
      case 1:
      case 2:
      case 5:
      case 7:
      case 8:
      case 9:
        return *((lsa_float*)result);
      default:
        return 0;
    }
  } else {
    return -1;
  }
}

lsa_int anfopar_ColCount = 10;
lsa_int anfopar_getColCount()
{
  return anfopar_ColCount;
}

lsa_int anfopar_getRowCount()
{
  return getRowCount(1);
}

Det getanfopar_Detektor_Anfo( int row )
{
  Det* result = detOfNr( anfopar[row].Detektor_Anfo );
  if (result != NULL) {
    return *result;
  } else {
    return -1;
  }
}

void* getValuefunkpar( lsa_int spalte, lsa_int zeile )
{
  switch (spalte)
  {
    case 0: return detOfNr( funkpar[zeile].Detektor_Funktion );
    case 1: return &funkpar[zeile].inteff;
    case 2: return &funkpar[zeile].maxfor;
    case 3: return &funkpar[zeile].fejl;
    default: return NULL;
  }
}

lsa_int getfunkpar(lsa_int spalte, lsa_int zeile) {
  void* result = getValuefunkpar(spalte, zeile);
  if (result != NULL) {
    switch (spalte) {
      case 0:
      case 3:
        return *((lsa_int*)result);
      case 1:
      case 2:
        return *((lsa_float*)result);
      default:
        return 0;
    }
  } else {
    return -1;
  }
}

lsa_int funkpar_ColCount = 4;
lsa_int funkpar_getColCount()
{
  return funkpar_ColCount;
}

lsa_int funkpar_getRowCount()
{
  return getRowCount(2);
}

Det getfunkpar_Detektor_Funktion( int row )
{
  Det* result = detOfNr( funkpar[row].Detektor_Funktion );
  if (result != NULL) {
    return *result;
  } else {
    return -1;
  }
}

void* getValuepx( lsa_int spalte, lsa_int zeile )
{
  switch (spalte)
  {
    case 0: return phaOfNr( px[zeile].phd );
    case 1: return &px[zeile].min;
    case 2: return &px[zeile].forl;
    case 3: return &px[zeile].frat;
    default: return NULL;
  }
}

lsa_int getpx(lsa_int spalte, lsa_int zeile) {
  void* result = getValuepx(spalte, zeile);
  if (result != NULL) {
    switch (spalte) {
      case 0:
        return *((lsa_int*)result);
      case 1:
      case 2:
      case 3:
        return *((lsa_float*)result);
      default:
        return 0;
    }
  } else {
    return -1;
  }
}

lsa_int px_ColCount = 4;
lsa_int px_getColCount()
{
  return px_ColCount;
}

lsa_int px_getRowCount()
{
  return getRowCount(6);
}

Pha getpx_phd( int row )
{
  Pha* result = phaOfNr( px[row].phd );
  if (result != NULL) {
    return *result;
  } else {
    return -1;
  }
}


void initLogikParameter()
{
  EXT_initLogikParameter();
  initLogikParameterTyp( uuidVerlpar, (void**)&verlpar );
  initLogikParameterTyp( uuidAnfopar, (void**)&anfopar );
  initLogikParameterTyp( uuidFunkpar, (void**)&funkpar );
  initLogikParameterTyp( uuidPx, (void**)&px );
}


void EXT_runPost() {}


void initOMTCObjekte()
{
  tks = tkObjs;
  ausgs = ausgObjs;
  sgrs = sgrObjs;
  dets = detObjs;
  mps = NULL;
  oevs = NULL;
  spls = splObjs;
  rhms = NULL;
  tims = timObjs;
  phas = phaObjs;
  pues = pueObjs;
  modifs = NULL;
  logikParaStrukturen = _parameterblockStructures;
  logikParaStrukturenCount = 7;
  verfahrenObjs[0].getNameVerfahren = EXT_getNameVerfahren;
  verfahrenObjs[0].getVersionVerfahren = EXT_getVersionVerfahren;
  verfahrenObjs[0].runMain = EXT_runMain;
  verfahrenObjs[0].runPost =  EXT_runPost;
  verfahren = verfahrenObjs;
  verfahrenCount = 1;
}


#endif

