#ifndef PARLGDEF_INCLUDED
#define PARLGDEF_INCLUDED


#define verlpar_getvalue(_LisaPara1,_LisaPara2) getValueverlpar(_LisaPara1, _LisaPara2)
#define verlpar_get(_LisaPara1,_LisaPara2) getverlpar(_LisaPara1, _LisaPara2)
#define verlpar_Detektor_Anfo_get(_LisaPara1) getverlpar_Detektor_Anfo(_LisaPara1)
#define verlpar_getDetektor_Anfo(_LisaPara1) getverlpar_Detektor_Anfo(_LisaPara1)
#define verlpar_Detektor_Anfo(_LisaPara1) getverlpar_Detektor_Anfo(_LisaPara1)
#define verlpar_staint(_LisaPara1) verlpar[_LisaPara1].staint
#define verlpar_sluint(_LisaPara1) verlpar[_LisaPara1].sluint
#define verlpar_fratid(_LisaPara1) verlpar[_LisaPara1].fratid
#define verlpar_udkobl(_LisaPara1) verlpar[_LisaPara1].udkobl
#define verlpar_opm(_LisaPara1) verlpar[_LisaPara1].opm
#define verlpar_opmfbi(_LisaPara1) verlpar[_LisaPara1].opmfbi
#define verlpar_maxo(_LisaPara1) verlpar[_LisaPara1].maxo
#define verlpar_gen(_LisaPara1) verlpar[_LisaPara1].gen
#define anfopar_getvalue(_LisaPara1,_LisaPara2) getValueanfopar(_LisaPara1, _LisaPara2)
#define anfopar_get(_LisaPara1,_LisaPara2) getanfopar(_LisaPara1, _LisaPara2)
#define anfopar_Detektor_Anfo_get(_LisaPara1) getanfopar_Detektor_Anfo(_LisaPara1)
#define anfopar_getDetektor_Anfo(_LisaPara1) getanfopar_Detektor_Anfo(_LisaPara1)
#define anfopar_Detektor_Anfo(_LisaPara1) getanfopar_Detektor_Anfo(_LisaPara1)
#define anfopar_retnin(_LisaPara1) anfopar[_LisaPara1].retnin
#define anfopar_udbala(_LisaPara1) anfopar[_LisaPara1].udbala
#define anfopar_anmfor(_LisaPara1) anfopar[_LisaPara1].anmfor
#define anfopar_pastil(_LisaPara1) anfopar[_LisaPara1].pastil
#define anfopar_anmses(_LisaPara1) anfopar[_LisaPara1].anmses
#define anfopar_hukomm(_LisaPara1) anfopar[_LisaPara1].hukomm
#define anfopar_forsin(_LisaPara1) anfopar[_LisaPara1].forsin
#define anfopar_udskud(_LisaPara1) anfopar[_LisaPara1].udskud
#define anfopar_prrese(_LisaPara1) anfopar[_LisaPara1].prrese
#define funkpar_getvalue(_LisaPara1,_LisaPara2) getValuefunkpar(_LisaPara1, _LisaPara2)
#define funkpar_get(_LisaPara1,_LisaPara2) getfunkpar(_LisaPara1, _LisaPara2)
#define funkpar_Detektor_Funktion_get(_LisaPara1) getfunkpar_Detektor_Funktion(_LisaPara1)
#define funkpar_getDetektor_Funktion(_LisaPara1) getfunkpar_Detektor_Funktion(_LisaPara1)
#define funkpar_Detektor_Funktion(_LisaPara1) getfunkpar_Detektor_Funktion(_LisaPara1)
#define funkpar_inteff(_LisaPara1) funkpar[_LisaPara1].inteff
#define funkpar_maxfor(_LisaPara1) funkpar[_LisaPara1].maxfor
#define funkpar_fejl(_LisaPara1) funkpar[_LisaPara1].fejl
#define px_getvalue(_LisaPara1,_LisaPara2) getValuepx(_LisaPara1, _LisaPara2)
#define px_get(_LisaPara1,_LisaPara2) getpx(_LisaPara1, _LisaPara2)
#define px_phd_get(_LisaPara1) getpx_phd(_LisaPara1)
#define px_getphd(_LisaPara1) getpx_phd(_LisaPara1)
#define px_getPhd(_LisaPara1) getpx_phd(_LisaPara1)
#define px_phd(_LisaPara1) getpx_phd(_LisaPara1)
#define px_min(_LisaPara1) px[_LisaPara1].min
#define px_forl(_LisaPara1) px[_LisaPara1].forl
#define px_frat(_LisaPara1) px[_LisaPara1].frat


#endif

