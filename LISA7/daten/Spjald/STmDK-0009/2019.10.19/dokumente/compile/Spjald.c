/**
 * OMTC Control logic.
 * test - 
 * Spjald
 * @author grote_W10
 * @version Spjald/STmDK-0009/2019.10.19
 * @lastchange grote_W10/19-10-2019  09:26:11
 * @generator LISA 7.2.0.729, SCHLOTHAUER & WAUER GmbH
 * @library $Controller: OML, $Version: 2.0, $Name: LISA OML 2.0, $File: C:/Program Files (x86)/LISA71/LISA_OMTC/StvOML200.jar
$Controller: OML, $Version: Math Library, $Name: Math Library, $File: C:/Program Files (x86)/LISA71/LISA_OMTC/MathLISA100.jar
$Controller: OML, $Version: Math Library, $Name: Math Library, $File: C:/Program Files (x86)/LISA71/LISA_OMTC/MathLISA100.jar$Controller: OML, $Version: OML Extended 1.2, $Name: OML Extended 1.2, $File: C:/Program Files (x86)/LISA71/LISA_OMTC/OMLExtended120.jar $
 */

#include "omtcItf.h"
#include "omtcCon.h"
#include "pardef.h"
#include "parlgdef.h"
#include "MathLISA.h"
#include "extImpl.h"
#include "extFunc.h"
#include "extPlgd.h"
#include "extPar.h"
#include "Spjald.h"

  /* Signal timing plans */
  Spl P1;
  Spl P8;

  /* Stages */
  Pha Styreblokke_1;
  Pha Styreblokke_2;

  /* Stage transitions */
  Pue PUe_1_2;
  Pue PUe_2_1;

  /* Signal groups */
  Sgr A1;
  Sgr A2;
  Sgr B1;
  Sgr B2;
  Sgr af;
  Sgr ag;
  Sgr bf;
  Sgr bg;

  /* Detectors */
  Det D1;
  Det D2;
  Det D3;
  Det D4;
  Det D5a;
  Det D5b;
  Det D6;
  Det D7a;
  Det D7b;
  Det D8;
  Det Fbf;
  Det Fbg;

  /* Timers */
  Tim Anf_VZ_D1;
  Tim Anf_VZ_D13;
  Tim Anf_VZ_D16;
  Tim Anf_VZ_D18;
  Tim Anf_VZ_D7;
  Tim timer_ZZ;

  /* Auxiliary signals */
  Ausg Kvittering_bf;
  Ausg Kvittering_bg;

  /* Sub-intersections */
  Tk LsaTK_1;

  /* Global variables */
  lsa_bool Anfo_B1;
  lsa_bool Anfo_B2;
  lsa_bool Anfo_bf;
  lsa_bool Anfo_bg;
  lsa_bool ASG;
  lsa_bool dummy_boo;
  lsa_int Fail_D1;
  lsa_int Fail_D2;
  lsa_int Fail_D3;
  lsa_int Fail_D4;
  lsa_int Fail_D5;
  lsa_int Fail_D6;
  lsa_int Fail_D7;
  lsa_int Fail_D8;
  lsa_float forl;
  lsa_float frat;
  lsa_bool M_VL1a;
  lsa_float MAXWZ;
  Sgr MAXWZ_SGR;
  lsa_float min;
  lsa_float min_Opma_A1;
  lsa_float min_Opma_A2;
  lsa_int Periode;
  lsa_int PZ;
  lsa_float SG_WZ[20];
  lsa_float VZ;
  lsa_float WARTE_MAX;
  Sgr WARTE_MAX_SG;
  lsa_int WZ_bf;
  lsa_int WZ_bg;
  lsa_float Zeitluecke_D1;
  lsa_float Zeitluecke_D2;
  lsa_float Zeitluecke_D3;
  lsa_float Zeitluecke_D4;
  lsa_float Zeitluecke_D5a;
  lsa_float Zeitluecke_D6;
  lsa_float Zeitluecke_D7a;
  lsa_float Zeitluecke_D8;
  lsa_bool ZZV;
  lsa_float Test;
  lsa_bool ANF_D5;
  lsa_bool ANF_D7;

#include "Spjald.h"

void newInitLogik(void)
{
  char szElementName[13];
  int i1;
  /* Initialize global variables */
  Anfo_B1 = factoryNewBoolean( "Anfo_B1", &Anfo_B1, false, 32, 0x02 );
  Anfo_B2 = factoryNewBoolean( "Anfo_B2", &Anfo_B2, false, 32, 0x02 );
  Anfo_bf = factoryNewBoolean( "Anfo_bf", &Anfo_bf, false, 32, 0x02 );
  Anfo_bg = factoryNewBoolean( "Anfo_bg", &Anfo_bg, false, 32, 0x02 );
  ASG = factoryNewBoolean( "ASG", &ASG, false, 32, 0x02 );
  dummy_boo = factoryNewBoolean( "dummy_boo", &dummy_boo, false, 0, 0x02 );
  Fail_D1 = factoryNewInt( "Fail_D1", &Fail_D1, 0, 0, 0x02 );
  Fail_D2 = factoryNewInt( "Fail_D2", &Fail_D2, 0, 0, 0x02 );
  Fail_D3 = factoryNewInt( "Fail_D3", &Fail_D3, 0, 0, 0x02 );
  Fail_D4 = factoryNewInt( "Fail_D4", &Fail_D4, 0, 0, 0x02 );
  Fail_D5 = factoryNewInt( "Fail_D5", &Fail_D5, 0, 0, 0x02 );
  Fail_D6 = factoryNewInt( "Fail_D6", &Fail_D6, 0, 0, 0x02 );
  Fail_D7 = factoryNewInt( "Fail_D7", &Fail_D7, 0, 0, 0x02 );
  Fail_D8 = factoryNewInt( "Fail_D8", &Fail_D8, 0, 0, 0x02 );
  forl = factoryNewFloat( "forl", &forl, 0.0f, 32, 0x02 );
  frat = factoryNewFloat( "frat", &frat, 0.0f, 32, 0x02 );
  M_VL1a = factoryNewBoolean( "M_VL1a", &M_VL1a, false, 32, 0x02 );
  MAXWZ = factoryNewFloat( "MAXWZ", &MAXWZ, 0.0f, 0, 0x02 );
  MAXWZ_SGR = factoryNewInt( "MAXWZ_SGR", &MAXWZ_SGR, -1, 32, 0x02 );
  min = factoryNewFloat( "min", &min, 0.0f, 32, 0x02 );
  min_Opma_A1 = factoryNewFloat( "min_Opma_A1", &min_Opma_A1, 0.0f, 32, 0x02 );
  min_Opma_A2 = factoryNewFloat( "min_Opma_A2", &min_Opma_A2, 0.0f, 32, 0x02 );
  Periode = factoryNewInt( "Periode", &Periode, 0, 32, 0x02 );
  PZ = factoryNewInt( "PZ", &PZ, 0, 32, 0x02 );
  for (i1 = 0; i1 < 20; i1++) {
    sprintf( szElementName, "SG_WZ[%d-20]", i1 );
    SG_WZ[i1] = factoryNewFloat( szElementName, &SG_WZ[i1], 0, 32, 0x02 );
  }
  VZ = factoryNewFloat( "VZ", &VZ, 0.0f, 32, 0x02 );
  WARTE_MAX = factoryNewFloat( "WARTE_MAX", &WARTE_MAX, 0.0f, 32, 0x02 );
  WARTE_MAX_SG = factoryNewInt( "WARTE_MAX_SG", &WARTE_MAX_SG, -1, 32, 0x02 );
  WZ_bf = factoryNewInt( "WZ_bf", &WZ_bf, 0, 32, 0x02 );
  WZ_bg = factoryNewInt( "WZ_bg", &WZ_bg, 0, 32, 0x02 );
  Zeitluecke_D1 = factoryNewFloat( "Zeitluecke_D1", &Zeitluecke_D1, 0.0f, 32, 0x02 );
  Zeitluecke_D2 = factoryNewFloat( "Zeitluecke_D2", &Zeitluecke_D2, 0.0f, 32, 0x02 );
  Zeitluecke_D3 = factoryNewFloat( "Zeitluecke_D3", &Zeitluecke_D3, 0.0f, 32, 0x02 );
  Zeitluecke_D4 = factoryNewFloat( "Zeitluecke_D4", &Zeitluecke_D4, 0.0f, 32, 0x02 );
  Zeitluecke_D5a = factoryNewFloat( "Zeitluecke_D5a", &Zeitluecke_D5a, 0.0f, 32, 0x02 );
  Zeitluecke_D6 = factoryNewFloat( "Zeitluecke_D6", &Zeitluecke_D6, 0.0f, 32, 0x02 );
  Zeitluecke_D7a = factoryNewFloat( "Zeitluecke_D7a", &Zeitluecke_D7a, 0.0f, 32, 0x02 );
  Zeitluecke_D8 = factoryNewFloat( "Zeitluecke_D8", &Zeitluecke_D8, 0.0f, 32, 0x02 );
  ZZV = factoryNewBoolean( "ZZV", &ZZV, false, 32, 0x02 );
  Test = factoryNewFloat( "Test", &Test, 0.0f, 0, 0x02 );
  ANF_D5 = factoryNewBoolean( "ANF_D5", &ANF_D5, false, 0, 0x02 );
  ANF_D7 = factoryNewBoolean( "ANF_D7", &ANF_D7, false, 0, 0x02 );

  /* Signal timing plans */
  P1 = spl_register("P1", 0);
  P8 = spl_register("P8", 1);

  /* Stages */
  Styreblokke_1 = pha_register("Styreblokke 1", 0);
  Styreblokke_2 = pha_register("Styreblokke 2", 1);

  /* Stage transitions */
  PUe_1_2 = pue_register("P� 1.2", 0);
  PUe_2_1 = pue_register("P� 2.1", 1);

  /* Signal groups */
  A1 = sgr_register("A1", 0);
  A2 = sgr_register("A2", 1);
  B1 = sgr_register("B1", 2);
  B2 = sgr_register("B2", 3);
  af = sgr_register("af", 4);
  ag = sgr_register("ag", 5);
  bf = sgr_register("bf", 6);
  bg = sgr_register("bg", 7);

  /* Detectors */
  D1 = det_register("D1", 0);
  D2 = det_register("D2", 1);
  D3 = det_register("D3", 2);
  D4 = det_register("D4", 3);
  D5a = det_register("D5a", 4);
  D5b = det_register("D5b", 5);
  D6 = det_register("D6", 6);
  D7a = det_register("D7a", 7);
  D7b = det_register("D7b", 8);
  D8 = det_register("D8", 9);
  Fbf = det_register("Fbf", 10);
  Fbg = det_register("Fbg", 11);

  /* Timers */
  Anf_VZ_D1 = tim_register("Anf_VZ_D1", 0);
  Anf_VZ_D13 = tim_register("Anf_VZ_D13", 1);
  Anf_VZ_D16 = tim_register("Anf_VZ_D16", 2);
  Anf_VZ_D18 = tim_register("Anf_VZ_D18", 3);
  Anf_VZ_D7 = tim_register("Anf_VZ_D7", 4);
  timer_ZZ = tim_register("timer_ZZ", 5);

  /* Auxiliary signals */
  Kvittering_bf = ausg_register("Kvittering_bf", 0);
  Kvittering_bg = ausg_register("Kvittering_bg", 1);

  /* Sub-intersections */
  LsaTK_1 = tk_register("LsaTK_1", 0);
}

  void runHauptLogik( void )
  {
    Hauptlogik();
  }

  void runInitLogik( lsa_int initMode )
  {
    Init( initMode );
  }

  void runVorLogik( void )
  {
    Vorlogik();
  }

  void runPostLogik( void )
  {
    Nachlogik();
  }

  void runUmschaltLogik( void )
  {
    Umschaltung();
  }

  /****************
   *  Hauptlogik
   ****************/
  void Hauptlogik( void ) 
  {
    /* UP_Opma( A1, D3, min_Opma_A1 );
     UP_Opma( A2, D6, min_Opma_A2 ); */
    min_max();
    akt_Dauer_phase( &VZ );
    if (PhaGet( Styreblokke_1 ))
    {
      PZ = UP_Styreblokke_1();
    }
    else
    {
      if (PhaGet( Styreblokke_2 ))
      {
        PZ= UP_Styreblokke_2();
      }
    }
    UP_Anz_ZL( A1, D1, &Zeitluecke_D1 );
    UP_Anz_ZL( A1, D2, &Zeitluecke_D2 );
    UP_Anz_ZL( A2, D3, &Zeitluecke_D3 );
    UP_Anz_ZL( A2, D4, &Zeitluecke_D4 );
    UP_Anz_ZL( B1, D5a, &Zeitluecke_D5a );
    UP_Anz_ZL( B1, D6, &Zeitluecke_D6 );
    UP_Anz_ZL( B2, D7a, &Zeitluecke_D7a );
    UP_Anz_ZL( B2, D8, &Zeitluecke_D8 );
    return;
  }

  /*************
   *  UP_Anfo
   *************/
  void UP_Anfo( void ) 
  {
    Anfo_B1 = FRE( &D5a, &D5b, Fail_D5, B1, &dummy_boo, &ANF_D5 ) || Anmeldelser( &D6, Fail_D6, B1, &dummy_boo );
    Anfo_B2 = FRE( &D7a, &D7b, Fail_D7, B2, &dummy_boo, &ANF_D7 ) || Anmeldelser( &D8, Fail_D8, B2, &dummy_boo );
    SG_Wartezeit();
    WZ_bf = (int) SG_WZ[ SgrIndex( bf ) ];
    WZ_bg = (int) SG_WZ[ SgrIndex( bg ) ];
    ASG = Anfo_B1 || Anfo_B2 || WZ_bf > 0 || WZ_bg > 0 || SgrSperr( af ) > 0;
    Anfo_bf = WZ_bf > 0;
    Anfo_bg = WZ_bg > 0;
    return;
  }

  /**************
   *  Vorlogik
   **************/
  void Vorlogik( void ) 
  {
    DST_Erkennung();
    /* Logiktest */
    UP_Anfo();
    ZZV = false;
    return;
  }

  /*****************
   *  Umschaltung
   *****************/
  void Umschaltung( void ) 
  {
    LbsUmschaltStatusSet (LBS_cUpAuto);
    if (! (LbsBetriebszustandTK( LsaTK_1 ) == LBS_cLsaSplEin))
    {
      return;
    }
    else
    {
      if ((LbsModifZustandWunsch() & LBS_cMzFzs) == LBS_cMzFzs || LbsBetriebszustandTK( LsaTK_1 ) == 1 || LbsBetriebszustandTKWunsch( LsaTK_1 ) == 1 || LbsBetriebszustandTK( LsaTK_1 ) == 2 || LbsBetriebszustandTKWunsch( LsaTK_1 ) == 2 || LbsBetriebszustandTK( LsaTK_1 ) == 3 || LbsBetriebszustandTKWunsch( LsaTK_1 ) == 3)
      {
        if (SgrFrei( A1 ) < 1 || SgrFrei( A2 ) < 1 || SgrFrei( af ) < 1 || SgrFrei( ag ) < 1)
        {
          LbsUmschaltStatusSet (LBS_cUpNichtErlaubt);
          return;
        }
        else
        {
          /* LbsUmschaltStatusSet (LBS_cUpErlaubt) */
          return;
        }
      }
      else
      {
        return;
      }
    }
  }

  /**********************
   *  UP_Styreblokke_1
   **********************/
  lsa_int UP_Styreblokke_1( void ) 
  {
    M_VL1a = false;
    if (PhaDauer( Styreblokke_1 ) == 0)
    {
      LbsTxSet( 0 );
    }
    if (VZ < min)
    {
      return 1;
    }
    else
    {
      if (! ASG && Gruen( af ) && Gruen( ag ))
      {
        PhaDauerSet( Styreblokke_1, min - 1 );
        return 2;
      }
      else
      {
        if (minfrei( af ))
        {
        }
        else
        {
          SgrAb( af );
        }
        if (minfrei( ag ) || SgrSperr( af ) < 2)
        {
        }
        else
        {
          SgrAb( ag );
        }
        if (VL_Det( D1, Fail_D1, A1 ) || VL_Det( D2, Fail_D2, A1 ) || VL_Det( D3, Fail_D3, A2 ) || VL_Det( D4, Fail_D4, A2 ) || Gruen( af ) || Gruen( ag ))
        {
          return 3;
        }
        else
        {
          if (NE( PUe_1_2 ))
          {
            return 4;
          }
          else
          {
            PueSet( PUe_1_2 );
            return 5;
          }
        }
      }
    }
  }

  /***************
   *  Nachlogik
   ***************/
  void Nachlogik( void ) 
  {
    /* Local variables */
    lsa_int i = factoryNewLocalInt( &i, 0 );
    Det det_ = factoryNewLocalInt( &det_, -1 );
    Ausg aus_ = factoryNewLocalInt( &aus_, -1 );
    SK( Fbf, Fbf, bf, Kvittering_bf );
    SK( Fbg, Fbg, bg, Kvittering_bg );
    if (LbsBetriebszustandTK( LsaTK_1 ) == 1 || LbsBetriebszustandTKWunsch( LsaTK_1 ) == 1 || LbsBetriebszustandTK( LsaTK_1 ) == 2 || LbsBetriebszustandTKWunsch( LsaTK_1 ) == 2 || LbsBetriebszustandTK( LsaTK_1 ) == 3 || LbsBetriebszustandTKWunsch( LsaTK_1 ) == 3)
    {
      for (i = 0; i <= AusgCount() -1; i++)
      {
        aus_ = AusgItem( i );
        if (AusgTk( aus_ ) == LsaTK_1)
        {
          AusgSet( aus_, AUSG_cAusgAus );
        }
      }
    }
    if (LbsBetriebszustandTK( LsaTK_1 ) == 1 || LbsBetriebszustandTK( LsaTK_1 ) == 2 || LbsBetriebszustandTK( LsaTK_1 ) == 3)
    {
      for (i = 0; i <= DetCount() -1; i++)
      {
        det_ = DetItem( i );
        DetAnfoSet( det_, false );
      }
    }
    return;
  }

  /*********************
   *  Init
   *  @param initMode
   *********************/
  void Init( lsa_int initMode ) 
  {
    if (! (LbsBetriebszustandTK( LsaTK_1 ) == LBS_cLsaSplEin))
    {
      return;
    }
    else
    {
      PhaDauerSet( Styreblokke_1, SgrFrei( A1 ) );
      DetAnfoSet( Fbf, true );
      DetAnfoSet( Fbg, true );
      return;
    }
  }

  /*******************
   *  DST_Erkennung
   *******************/
  void DST_Erkennung( void ) 
  {
    /* Local variables */
    lsa_int d = factoryNewLocalInt( &d, 0 );
    Det Detektor = factoryNewLocalInt( &Detektor, -1 );
    Fail_D1 = 0;
    Fail_D2 = 0;
    Fail_D3 = 0;
    Fail_D4 = 0;
    Fail_D5 = 0;
    Fail_D6 = 0;
    Fail_D7 = 0;
    Fail_D8 = 0;
    for (d = 0; d <= DetCount() - 1; d++)
    {
      Detektor = DetItem( d );
      if (DetStoer( Detektor ))
      {
        if (DetIndex( Detektor ) == 0)
        {
          Fail_D1 = funkpar_fejl( D1 );
        }
        if (DetIndex( Detektor ) == 1)
        {
          Fail_D2 = funkpar_fejl( D2 );
        }
        if (DetIndex( Detektor ) == 2)
        {
          Fail_D3 = funkpar_fejl( D3 );
        }
        if (DetIndex( Detektor ) == 3)
        {
          Fail_D4 = funkpar_fejl( D4 );
        }
        if (DetIndex( Detektor ) == 4)
        {
          Fail_D5 = funkpar_fejl( D5a );
        }
        if (DetIndex( Detektor ) == 5)
        {
          Fail_D5 = funkpar_fejl( D5b );
        }
        if (DetIndex( Detektor ) == 6)
        {
          Fail_D6 = funkpar_fejl( D6 );
        }
        if (DetIndex( Detektor ) == 7)
        {
          Fail_D7 = funkpar_fejl( D7a );
        }
        if (DetIndex( Detektor ) == 8)
        {
          Fail_D7 = funkpar_fejl( D7b );
        }
        if (DetIndex( Detektor ) == 9)
        {
          Fail_D8 = funkpar_fejl( D8 );
        }
      }
    }
    return;
  }

  /*************
   *  min_max
   *************/
  void min_max( void ) 
  {
    /* Local variables */
    lsa_int p = factoryNewLocalInt( &p, 0 );
    lsa_int i = factoryNewLocalInt( &i, 0 );
    Pha ph = factoryNewLocalInt( &ph, -1 );
    for (p = 0; p <= px_getRowCount() - 1; p++)
    {
      if (PhaGet( Styreblokke_1 ))
      {
        if (px_getPhd( p ) == Styreblokke_1)
        {
          min = px_min( p );
          forl = px_forl( p );
          frat = px_frat( p );
          return;
        }
      }
      if (PhaGet( Styreblokke_2 ))
      {
        if (px_getPhd( p ) == Styreblokke_2)
        {
          min = px_min( p );
          forl = px_forl( p );
          frat = px_frat( p );
          return;
        }
      }
    }
    return;
  }

  /**********************
   *  UP_Styreblokke_2
   **********************/
  lsa_int UP_Styreblokke_2( void ) 
  {
    if (VZ < min)
    {
      return 1;
    }
    else
    {
      if (minfrei( bf ))
      {
      }
      else
      {
        SgrAb( bf );
      }
      if (minfrei( bg ))
      {
      }
      else
      {
        SgrAb( bg );
      }
      if (VL_Det( D5a, Fail_D5, B1 ) || VL_Det( D6, Fail_D6, B1 ) || VL_Det( D7a, Fail_D7, B2 ) || VL_Det( D8, Fail_D8, B2 ) || Gruen( bf ) || Gruen( bg ))
      {
        return 2;
      }
      else
      {
        if (NE( PUe_2_1 ))
        {
          return 3;
        }
        else
        {
          PueSet( PUe_2_1 );
          return 4;
        }
      }
    }
  }

  /*********************
   *  akt_Dauer_phase
   *  @param VZ_
   *********************/
  void akt_Dauer_phase( lsa_float *VZ_ ) 
  {
    /* Local variables */
    lsa_int i = factoryNewLocalInt( &i, 0 );
    Pha ph = factoryNewLocalInt( &ph, -1 );
    for (i = 0; i <= PhaCount() - 1; i++)
    {
      ph = PhaItem( i );
      if (PhaGet( ph ))
      {
        if (ph != -1)
        {
          *VZ_ = PhaDauer( ph );
        }
        else
        {
          *VZ_ = 0;
        }
      }
    }
    return;
  }

  /***************
   *  Gruen
   *  @param SG
   ***************/
  lsa_bool Gruen( Sgr SG ) 
  {
    if (SgrWunschFarbbild(SG) != SgrFbFrei(SG))
    {
      return false;
    }
    else
    {
      return true;
    }
  }

  /**********************
   *  NE
   *  @param Phasenpue
   **********************/
  lsa_bool NE( Pue Phasenpue ) 
  {
    /* Local variables */
    lsa_int i = factoryNewLocalInt( &i, 0 );
    Sgr Signal = factoryNewLocalInt( &Signal, -1 );
    lsa_float minSg = factoryNewLocalFloat( &minSg, 0.0f );
    lsa_float freiSg = factoryNewLocalFloat( &freiSg, 0.0f );
    lsa_float anerl = factoryNewLocalFloat( &anerl, 0.0f );
    for (i = 0; i <= SgrCount() -1; i++)
    {
      Signal = SgrItem( i );
      if (LbsBetriebszustandTK( SgrTk( Signal ) ) != LBS_cLsaSplEin)
      {
      }
      else
      {
        if (SgrWunschFarbbild(Signal) != SgrFbFrei(Signal))
        {
        }
        else
        {
          minSg = SbefNextT( PueSBef( Phasenpue ), Signal, 0, SgrFbSperr( Signal ) );
          if ((minSg + SgrFrei( Signal ) < SgrFreiMin( Signal ) || minSg + SgrFrei( Signal ) < ext_t_g1 (Signal)) && minSg >= 0)
          {
            return true;
          }
        }
        freiSg = SbefNextT( PueSBef( Phasenpue ), Signal, 0, SgrFbFrei( Signal ) );
        if (freiSg == -1)
        {
        }
        else
        {
          if (SgrUebDauer( Signal, SGR_FbGruen, SGR_FbRot ) > 0)
          {
            if ((SgrSperr( Signal ) <  (SgrUebDauer( Signal, SGR_FbGruen, SGR_FbRot ) + 1)) && SgrWunschFarbbild(Signal) != SgrFbFrei(Signal))
            {
              return true;
            }
          }
          anerl = SgrAnErlDauer( Signal );
          if (anerl > 1000 && anerl < 2000 && SbefNextT( PueSBef( Phasenpue ), SgrItem( (int)(anerl-1000) ), 0, SgrFbSperr( SgrItem( (int)(anerl-1000) ) ) ) != -1)
          {
          }
          else
          {
            if (freiSg < anerl)
            {
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  /******************
   *  SG_Wartezeit
   ******************/
  void SG_Wartezeit( void ) 
  {
    /* Local variables */
    lsa_int i = factoryNewLocalInt( &i, 0 );
    Sgr s = factoryNewLocalInt( &s, -1 );
    lsa_int x = factoryNewLocalInt( &x, 0 );
    Det d = factoryNewLocalInt( &d, -1 );
    for (i = 0; i <= SgrCount() - 1; i++)
    {
      s = SgrItem( i );
      SG_WZ[ i ] = 0;
      for (x = 0; x <= DetCount() - 1; x++)
      {
        d = DetItem( x );
        if (DetSgr1( d ) == -1 && DetSgr2( d ) == -1)
        {
        }
        else
        {
          if (DetSgr1( d ) == s || DetSgr2( d ) == s)
          {
            if (DetAnfoDauer( d ) > SG_WZ[i])
            {
              SG_WZ[ i ] = DetAnfoDauer( d );
            }
          }
        }
      }
    }
    return;
  }

  /*********************
   *  SK
   *  @param anf_DET1
   *  @param anf_DET2
   *  @param SG_FG
   *  @param QUIT
   *********************/
  void SK( Det anf_DET1, Det anf_DET2, Sgr SG_FG, Ausg QUIT ) 
  {
    if (DetAnfo(anf_DET1) || DetAnfo( anf_DET2 ) || (LbsModifZustand() & LBS_cMzFzs) == LBS_cMzFzs)
    {
      AusgSet( QUIT, AUSG_cAusgEin );
    }
    if (SgrFreiZustand( SG_FG ) || SgrWunschFarbbild( SG_FG )==SGR_FbGruen)
    {
      AusgSet(QUIT,AUSG_cAusgAus);
    }
    return;
  }

  /*****************
   *  UP_Anz_ZL
   *  @param SG
   *  @param Det_
   *  @param ZLX
   *****************/
  void UP_Anz_ZL( Sgr SG, Det Det_, lsa_float *ZLX ) 
  {
    if (! Gruen( SG ))
    {
      if (*ZLX != 0)
      {
        return;
      }
      else
      {
        *ZLX = DetZl( Det_ );
        return;
      }
    }
    else
    {
      *ZLX = 0;
      return;
    }
  }

  /*********************
   *  VL_Det
   *  @param Det_
   *  @param FailDet_
   *  @param SG
   *********************/
  lsa_bool VL_Det( Det Det_, lsa_float FailDet_, Sgr SG ) 
  {
    if (SgrWunschFarbbild(SG) != SgrFbFrei(SG))
    {
      return false;
    }
    else
    {
      if (FailDet_ == 1 || FailDet_ == 4)
      {
        return false;
      }
      else
      {
        if (! ASG)
        {
          Periode = 1;
          if (DetZl( Det_ ) < verlpar_staint( Det_ ) || FailDet_ == 2 || FailDet_ == 3)
          {
            return true;
          }
          else
          {
            return false;
          }
        }
        else
        {
          if (VZ < forl)
          {
            Periode = 2;
            if (DetZl( Det_ ) < verlpar_sluint( Det_ ) || FailDet_ == 2 || FailDet_ == 3)
            {
              return true;
            }
            else
            {
              return false;
            }
          }
          else
          {
            if (VZ < forl + frat)
            {
              Periode = 3;
              if (DetZl( Det_ ) < verlpar_fratid( Det_ ) || FailDet_ == 2 || FailDet_ == 3)
              {
                return true;
              }
              else
              {
                return false;
              }
            }
            else
            {
              return false;
            }
          }
        }
      }
    }
  }

  /*******************
   *  minfrei
   *  @param Signal
   *******************/
  lsa_bool minfrei( Sgr Signal ) 
  {
    if (SgrWunschFarbbild(Signal) != SgrFbFrei(Signal))
    {
      return false;
    }
    else
    {
      if (SgrFrei( Signal ) < SgrFreiMin( Signal ))
      {
        return true;
      }
      else
      {
        if (SgrFrei( Signal ) < ext_t_g1 (Signal))
        {
          return true;
        }
        else
        {
          return false;
        }
      }
    }
  }

  /********************************
   *  FRE
   *  @param Det_v
   *  @param Det_n
   *  @param detfail_
   *  @param SG
   *  @param Merker_reservatiert
   *  @param M_Anfo
   ********************************/
  lsa_bool FRE( Det *Det_v, Det *Det_n, lsa_int detfail_, Sgr SG, lsa_bool *Merker_reservatiert, lsa_bool *M_Anfo ) 
  {
    DetAnfoSet( *Det_n, false );
    if (SgrSperr( SG ) <= anfopar_anmses( *Det_v ))
    {
      *Merker_reservatiert = false;
      *M_Anfo = false;
      DetAnfoSet( *Det_v, false );
      return false;
    }
    else
    {
      if (detfail_ == funkpar_fejl( *Det_v ))
      {
        DetAnfoSet( *Det_n, false );
        return false;
      }
      else
      {
        if (*Merker_reservatiert)
        {
          if (DetAnfoDauer( *Det_v ) -0.2f <= anfopar_prrese( *Det_v ) && DetZl( *Det_v ) <= anfopar_prrese( *Det_v ))
          {
            return false;
          }
          else
          {
            *Merker_reservatiert = false;
            DetAnfoSet( *Det_v, false );
            return false;
          }
        }
        else
        {
          if (DetZl( *Det_v ) >= anfopar_retnin( *Det_v ))
          {
            DetAnfoSet( *Det_v, false );
            return false;
          }
          else
          {
            if (DetZl( *Det_n ) >= 1 && ! *M_Anfo)
            {
              return false;
            }
            else
            {
              if (((DetBelegtDauer( *Det_n ) > DetBelegtDauer( *Det_v )  || ! DetBelegt( *Det_n )) && ! *M_Anfo))
              {
                DetAnfoSet( *Det_v, false );
                return false;
              }
              else
              {
                if (anfopar_anmfor( *Det_v ) == 4)
                {
                  if (anfopar_prrese( *Det_v ) != 0)
                  {
                    if (DetBelegtDauer( *Det_v ) > anfopar_prrese( *Det_v ))
                    {
                      return false;
                    }
                    else
                    {
                      *Merker_reservatiert = true;
                      return false;
                    }
                  }
                  else
                  {
                    *M_Anfo = true;
                    return true;
                  }
                }
                else
                {
                  *M_Anfo = true;
                  return true;
                }
              }
            }
          }
        }
      }
    }
  }

  /********************************
   *  Anmeldelser
   *  @param Det_
   *  @param detfail_
   *  @param SG
   *  @param Merker_reservatiert
   ********************************/
  lsa_bool Anmeldelser( Det *Det_, lsa_int detfail_, Sgr SG, lsa_bool *Merker_reservatiert ) 
  {
    if (SgrSperr( SG ) < anfopar_anmses( *Det_ ))
    {
      DetAnfoSet( *Det_, false );
      *Merker_reservatiert = false;
      return false;
    }
    else
    {
      if (detfail_ == 1 || detfail_ == 2)
      {
        DetAnfoSet( *Det_, false );
        *Merker_reservatiert = false;
        return false;
      }
      else
      {
        if (detfail_ == 3 || detfail_ == 4)
        {
          DetAnfoSet( *Det_, true );
          *Merker_reservatiert = false;
          return true;
        }
        else
        {
          if (*Merker_reservatiert)
          {
            if (DetAnfoDauer( *Det_ ) -0.2f <= anfopar_prrese( *Det_ ))
            {
              return false;
            }
            else
            {
              *Merker_reservatiert = false;
              DetAnfoSet( *Det_, false );
              return false;
            }
          }
          else
          {
            if (anfopar_prrese( *Det_ ) > 0)
            {
              if (DetAnfoDauer( *Det_ ) <= anfopar_prrese( *Det_ ) && DetAnfo( *Det_ ) && DetBelegtDauer( *Det_ ) <= anfopar_prrese( *Det_ ))
              {
                *Merker_reservatiert = true;
                return false;
              }
              else
              {
                return false;
              }
            }
            else
            {
              if (anfopar_anmfor( *Det_ ) == 1 || anfopar_anmfor( *Det_ ) == 3)
              {
                if (anfopar_udskud( *Det_ ) == 0)
                {
                  if (anfopar_pastil( *Det_ ) == 1 || anfopar_forsin( *Det_ ) == 0)
                  {
                    if (DetAnfo( *Det_ ))
                    {
                      if (anfopar_hukomm( *Det_ ) != 2)
                      {
                        return true;
                      }
                      else
                      {
                        DetAnfoSet( *Det_, false );
                        return true;
                      }
                    }
                    else
                    {
                      return false;
                    }
                  }
                  else
                  {
                    if (DetBelegtDauer( *Det_ ) >= anfopar_forsin( *Det_ ))
                    {
                      if (anfopar_hukomm( *Det_ ) != 2)
                      {
                        return true;
                      }
                      else
                      {
                        DetAnfoSet( *Det_, false );
                        return true;
                      }
                    }
                    else
                    {
                      DetAnfoSet( *Det_, false );
                      return false;
                    }
                  }
                }
                else
                {
                  if (DetAnfoDauer( *Det_ ) >= anfopar_udskud( *Det_ ))
                  {
                    if (anfopar_hukomm( *Det_ ) != 2)
                    {
                      return true;
                    }
                    else
                    {
                      DetAnfoSet( *Det_, false );
                      return true;
                    }
                  }
                  else
                  {
                    /* DetAnfoSet( Det_, false ) */
                    return false;
                  }
                }
              }
              else
              {
                if (anfopar_anmfor( *Det_ ) == 2)
                {
                  DetAnfoSet( *Det_, false );
                  return false;
                }
                else
                {
                  if (anfopar_anmfor( *Det_ ) == 4)
                  {
                    if (DetAnfo( *Det_ ))
                    {
                      if (anfopar_hukomm( *Det_ ) != 2)
                      {
                        return true;
                      }
                      else
                      {
                        DetAnfoSet( *Det_, false );
                        return true;
                      }
                    }
                    else
                    {
                      return true;
                    }
                  }
                  else
                  {
                    DetAnfoSet( *Det_, false );
                    return false;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
