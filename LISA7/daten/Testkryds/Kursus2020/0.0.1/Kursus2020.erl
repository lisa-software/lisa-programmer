ErlDat06<LISA><props><version>7.1.1.721</version><user>azt1232</user><date>44112.9996216319</date><kommentar visible="False"></kommentar></props><ErlaubData dversion="5"><erldat><erldatattr etlo="True"></erldatattr><pp><ppattr></ppattr></pp><ekali>
<ekaitli>
<erlkat><erlkatattr id="0" name="Permitted" erlvis="True"></erlkatattr>
<tbli><liid>-1</liid>
</tbli><tabdesc cada="Duration" cast="Beginning" caen="End" caze="zgr"></tabdesc><erlgraf shape="4" bgc="12632256" fgc="0" fbhg="16"></erlgraf></erlkat>
</ekaitli><ekaliattr lid="0"></ekaliattr></ekali><ppber><pp><ppattr p2="Permission periods" p9="False"></ppattr></pp></ppber>
<zeigli><liidz>-1</liidz>
</zeigli>
<erlberli>
<erlber><erbeattr id="7" dt="44074" name="TP_1_2" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="14" dt="44076" name="TP_1_3" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="15" dt="44076" name="TP_1_4" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="10" dt="44076" name="TP_1_5" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="11" dt="44076" name="EP_1_2" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="16" dt="44076" name="EP_1_3" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="17" dt="44076" name="EP_1_4" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="6" dt="44074" name="EP_1_5" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="1" dt="43944" name="EP_2" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="18" dt="44076" name="EP_3" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="19" dt="44076" name="EP_4" kid="0"></erbeattr><changestamp></changestamp></erlber>
<erlber><erbeattr id="4" dt="43944" name="EP_5" kid="0"></erbeattr><changestamp></changestamp></erlber><liidb>19</liidb>
</erlberli>
<plaene>
<erlplan><eplattr id="0" dt="43944" name="P1" tu="110" nr="1"></eplattr>
<ebli>
<ebrow><ebrattr bid="7"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="14"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="15"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="10"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="11"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="16"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="17"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="6"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="1"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="18"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="19"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
<ebrow><ebrattr bid="4"></ebrattr>
<ebitli>
</ebitli>
<zeigli>
</zeigli></ebrow>
</ebli><gerlplan><geplattr vis="1" fn="Arial" fh="-24" fc="-16777208" nacw="108"></geplattr><comit x="2" y="645" fn="Arial" fh="-16" vis="1"></comit><tit x="2" y="2" fn="Arial" fh="-37" text="P1" vis="1"></tit><sefo fn="Arial" fh="-16" fc="-16777208"></sefo><ttfo fn="Arial" fh="-19" fc="-16777208"></ttfo><legen vis="0" fn="Arial" fh="-19" fc="-16777208"></legen>
<erlroli>
<erlrow><erroattr id="11" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="1" rrid="10"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="10" text="TP_1_2" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="7"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="15" text="TP_1_3" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="14"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="16" text="TP_1_4" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="15"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="13" text="TP_1_5" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="10"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="14" text="EP_1_2" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="11"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="17" text="EP_1_3" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="16"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="18" text="EP_1_4" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="17"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="9" text="EP_1_5" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="6"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="2" text="EP_2" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="1"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="19" text="EP_3" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="18"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="20" text="EP_4" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="19"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="5" text="EP_5" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="2" eoid="4"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow>
<erlrow><erroattr id="12" vis="1" fn="Arial" fh="-24" fc="-16777208" kind="4" rrid="11"></erroattr>
<erlroli><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="0"></erlroliattr>
</erlroli></erlrow><erlroliattr vis="1" fn="Arial" fh="-24" fc="-16777208" liid="20"></erlroliattr>
</erlroli>
<vbid><li>0</li><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li><li>6</li><li>7</li><li>10</li><li>11</li><li>14</li><li>15</li><li>16</li><li>17</li><li>18</li><li>19</li>
</vbid></gerlplan><pp><ppattr p2="Permission plan P1" p7="1" p9="False"></ppattr></pp><kommentar></kommentar><changestamp><change_data><user>azt1232</user><time>44112.8735170833</time></change_data></changestamp></erlplan><liidp>9</liidp>
</plaene></erldat></ErlaubData></LISA>